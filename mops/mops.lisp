(defpackage #:mops
  (:use #:common-lisp)
  (:export 
   ;;; basic frame functions
   #:get-filler #:get-mop #:has-slots-p #:isa-p #:mop-search
   ;;; data loading
   #:kb-source #:load-kb
   ;;; dmap parser
   #:dmap #:references #:set-phrases
   )
  )

(in-package #:mops)

(defvar *mops* nil)
(defvar *absts* nil)
(defvar *source* nil)

;;; PUBLIC

(defun load-kb (pathname)
  (load-data pathname 'set-kb))

(defun get-mop (name)
  (assoc name (kb-mops)))

(defun isa-p (mop abst)
  (or (equal mop abst) ;; for data only in slots
      (member abst (assoc mop (kb-absts)))))

(defun get-filler (name role)
  (let ((slot (get-slot name role)))
    (values (slot-filler slot) (not (null slot)))))

(defun get-slot (name role)
  (some (lambda (abst) 
          (assoc role (mop-slots (get-mop abst))))
        (assoc name (kb-absts))))

;;; PRIVATE

(defun kb-mops () *mops*)
(defun kb-absts () *absts*)
(defun kb-source () *source*)

(defun mop-name (mop) (car mop))
(defun mop-absts (mop) (cadr mop))
(defun mop-slots (mop) (cddr mop))
(defun slot-role (slot) (car slot))
(defun slot-filler (slot) (cadr slot))

(defun load-data (pathname saver)
  (let ((lst nil))
    (with-open-file (in pathname)
      (do ((x (read in nil in) (read in nil in)))
          ((eql x in) 
           (funcall saver (nreverse lst) pathname)
           (length lst))
        (push x lst)))))

(defun set-kb (mops &optional source)
  (setf *source* source
        *mops* mops
        *absts* (linearize-mops))
  (length mops))

(defun linearize-mops ()
  (mapcar 'linearize (kb-mops)))

(defun linearize (mop)
  (remove-duplicates (all-absts (car mop))))

(defun all-absts (name)
  (cons name 
        (mapcan 'all-absts (cadr (get-mop name)))))
