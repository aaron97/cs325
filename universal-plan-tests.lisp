(in-package ddr-tests)

;;; 10/21/2015 Created file [CKR]

;;; Test cases for the Universal Planner

;;; These tests use the existing rules for Monkey and Bananas
;;; and Blocks world from DDR-PLAN.LISP
;;;
;;; You need to define the following additional rule sets
;;;
;;;  - *UNIVERSAL-PLAN-KB* -- the rules for a general planner
;;;    that avoids generates plans with repeated states;
;;;    Instead of STEP to select the best step, the planner should
;;;    use POSSIBLE to select possible steps.
;;;
;;;  - *MONKEY-POSSIBLE-KB* -- the POSSIBLE rules for the
;;;    monkey and bananas problem. These should just rule out
;;;    things like the monkey going from X to X, but should not
;;;    "direct" the monkey toward the goal
;;;
;;;  - *BLOCKS-WORLD-POSSIBLE-KB* -- the POSSIBLE rules for the
;;;    blocks world stacking problem. These should rule out
;;;    one obvious case: picking a block to push that's not
;;;    in the goal stack.
;;;
;;; These rules are used in the tests monkey-universal and
;;; blocks-world-universal

(define-test monkey-universal
    (init-kb (append *universal-plan-kb* *monkey-possible-kb* *monkey-kb* *room-kb*))
  
  
  (assert-true
   (ask '(plan ?plan
               (mb-state box-top center)
               (mb-state box-top center))
        ))
  
  (assert-true
   (ask '(plan ?plan
               (mb-state center center)
               (mb-state box-top center))
        ))
  
  (assert-true
   (ask '(plan ?plan
               (mb-state door window)
               (mb-state box-top center))
        ))
  
  (assert-false
   (ask '(plan ?plan
               (mb-state box-top center)
               (mb-state center center))
        ))  
  
  )

(define-test blocks-world-universal
    (init-kb (append *universal-plan-kb* *blocks-world-possible-kb* *blocks-world-kb*))
  
  (assert-true
   (ask '(plan ?x nil (cons a nil))))
  
  (assert-true
   (ask '(plan ?x (cons a nil) nil)))
  
  (assert-true
   (ask '(plan ?x (cons a nil) (cons b nil))))
  
  (assert-true
   (ask '(plan ?x (cons b (cons a nil)) (cons a (cons b nil)))
       ))
  
  (assert-true
   (ask '(plan ?x 
               (cons b (cons c (cons a nil))) 
               (cons c (cons b (cons a nil))))
       ))
  )