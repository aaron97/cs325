
;;; 09-17-14 dropped clp-exts [CKR]

(asdf:defsystem #:sddr
  :serial t
  :depends-on ("lisp-unit")
  :components ((:file "sddr")
               (:file "sddr-tests")))

