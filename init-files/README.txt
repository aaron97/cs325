These are init files for different versions of Lisp. Put the file for the version of Lisp you are using in your home directory. 

On Unix and MacOS, your home directory is ~/
On Windows, your home directory is /Users/username, e.g., /Users/smith

LispWorks Personal: use lw-init.Lisp
Allegro Common Lisp: use clinit.clinit
SBCL: use .sbclrc

SBCL and Allegro will autoload the init file when you start them.
LispWorks Personal does not. You will need to use the File | Load menu.